// implementation of AR-Experience (aka "World")
var World = {
    // you may request new data from server periodically, however: in this sample data is only requested once
    isRequestingData: false,

    // true once data was fetched
    initiallyLoadedData: false,

    // direction indicator asset
    markerDrawable_directionIndicator: null,

    // list of AR.GeoObjects that are currently shown in the scene / World
    markerList: [],

    // The last selected marker
    currentMarker: null,

    locationUpdateCounter: 0,
    updatePlacemarkDistancesEveryXLocationUpdates: 10,

    // GeoObjects scale
    geoObjectsScale: 1,


    // called at first to setup the world parameters
    loadSetupsFromJsonData : function loadSetupsFromJsonDataFn(setup)
    {
        // GeoObjectsScale
        World.geoObjectsScale = (2.5) * setup.screenwidth / 376;

        // show radar & set click-listener
        PoiRadar.show();
        $('#radarContainer').unbind('click');
        $("#radarContainer").click(PoiRadar.clickedRadar);
        // empty list of visible markers
        World.markerList = [];

        // start loading direction indicator assets
        World.markerDrawable_directionIndicator = new AR.ImageResource("assets/indi.png");

        // set ready to recieve pois data
        World.requestData();
    },



    /*
        Load or Erase Pois from World
    */
    // called to inject new POI data
    loadPoisFromJsonData: function loadPoisFromJsonDataFn(poiData) {
        // loop through POI-information and create an AR.GeoObject (=Marker) per POI
        for (var currentPlaceNr = 0; currentPlaceNr < poiData.length; currentPlaceNr++) {
            var singlePoi = {
                "id": poiData[currentPlaceNr].Id,
                "latitude": parseFloat(poiData[currentPlaceNr].Latitude),
                "longitude": parseFloat(poiData[currentPlaceNr].Longitude),
                "altitude": parseFloat(poiData[currentPlaceNr].Altitude),
                "type": poiData[currentPlaceNr].Type,
                "title": poiData[currentPlaceNr].Title,
                "thumbnail": poiData[currentPlaceNr].Thumbnail,
                "emoji": poiData[currentPlaceNr].Emoji
            };
            World.markerList.push(new Marker(singlePoi));
        }

        // updates distance information of all placemarks
        World.updateDistanceToUserValues();
    },

    // Called to erase Pois from world
    erasePoisFromJsonData: function erasePoisFromJsonData(poiData)
    {
        var erased = false;
        var j;
        for (var i = 0; i < poiData.length; currentPlaceNr++) {
            erased = false;
            j = 0;
            while(j < World.markerList.length && !erased) {
                if (poiData[i].Id == World.markerList[j].poidata.id)
                {
                    RemoveMarker(World.markerList[j]);
                    delete World.markerList[j];
                    erased = true;
                }
                j++;
            }
        }
    },


    // sets/updates distances of all makers so they are available way faster than calling (time-consuming) distanceToUser() method all the time
    updateDistanceToUserValues: function updateDistanceToUserValuesFn() {
        for (var i = 0; i < World.markerList.length; i++) {
            World.markerList[i].distanceToUser = World.markerList[i].markerObject.locations[0].distanceToUser();
        }
    },

    // location updates, fired every time you call architectView.setLocation() in native environment
    locationChanged: function locationChangedFn(lat, lon, alt, acc) {
        if (World.locationUpdateCounter === 0) {
            // update placemark distance information frequently, you max also update distances only every 10m with some more effort
            World.updateDistanceToUserValues();
        }

        // helper used to update placemark information every now and then (e.g. every 10 location upadtes fired)
        World.locationUpdateCounter = (++World.locationUpdateCounter % World.updatePlacemarkDistancesEveryXLocationUpdates);
      },

    // fired when user pressed maker in cam
    onMarkerSelected: function onMarkerSelectedFn(marker) {
        World.currentMarker = marker;
        var architectSdkUrl = "architectsdk://markerselected?id=" + encodeURIComponent(marker.poiData.id) ;
        document.location = architectSdkUrl;
    },

    // screen was clicked but no geo-object was hit
    onScreenClick: function onScreenClickFn() {
        // you may handle clicks on empty AR space too
    },

    // returns distance in meters of placemark with maxdistance * 1.1
    getMaxDistance: function getMaxDistanceFn() {

        // sort palces by distance so the first entry is the one with the maximum distance
        World.markerList.sort(World.sortByDistanceSortingDescending);

        // use distanceToUser to get max-distance
        var maxDistanceMeters = World.markerList[0].distanceToUser;

        // return maximum distance times some factor >1.0 so ther is some room left and small movements of user don't cause places far away to disappear
        return maxDistanceMeters * 1.1;
    },

    // request POI data from native environment
    requestData: function requestDataFn() {
        var architectSdkUrl_data = "architectsdk://requestdata";
        window.location = architectSdkUrl_data;
    },

    // request Setup data from native environment
    requestSetup: function requestSetupFn() {
        var architectSdkUrl = "architectsdk://requestsetup";
        window.location = architectSdkUrl;
    },

    // helper to sort places by distance
    sortByDistanceSorting: function(a, b) {
        return a.distanceToUser - b.distanceToUser;
    },

    // helper to sort places by distance, descending
    sortByDistanceSortingDescending: function(a, b) {
        return b.distanceToUser - a.distanceToUser;
    }

};


/* forward locationChanges to custom function */
AR.context.onLocationChanged = World.locationChanged;

/* forward clicks in empty area to World */
AR.context.onScreenClick = World.onScreenClick;