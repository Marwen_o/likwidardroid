var kMarker_AnimationDuration_ChangeDrawable = 500;
var kMarker_AnimationDuration_Resize = 1000;

// Drop types
var DROP_TYPE_CHALLENGE = "Challenge";
var DROP_TYPE_VIDEO = "Video";
var DROP_TYPE_IMAGE = "Image";

// the Drops' HtmlDrawables containt
var HTML_HEADER = "<head><meta name=\"viewport\" content=\"target-densitydpi=device-dpi,width=300,user-scalable=0\"/></head>";
var HTML_STYLE_IDLE = "<style type=\"text/css\">html{width:220px;max-height: 400px;}body{padding: 0px;margin:0px;}.div_marker{background-color: none;width: 200px;}.div_thumb{background-color: black;width: 200px;height: 200px;border-radius: 110px;border-width: 10px;border-color: red;border-style: solid;margin-left: 0px;padding-left: 0px;position: fixed;}.img_thumb{width: 220px;height: 220px;position: absolute;top:0px;left:-15px;}.img_emoji{width: 45px;height: 45px;position: absolute;top: 145px;left:160px;}.div_title{background-color: black;color: white;width: 220px;height: 300px;position: fixed;top: 116px;border-radius: 10px;}.div_title p{position : relative;top: 80px;color : white;font-size: 20px;}</style>";
var HTML_STYLE_SELECTED = "<style type=\"text/css\">.div_thumb_chlng_shadow{-webkit-box-shadow: 0px -17px 52px 7px rgba(5,41,247,0.75);-moz-box-shadow: 0px -17px 52px 7px rgba(5,41,247,0.75);box-shadow: 0px -17px 52px 7px rgba(5,41,247,0.75);}.div_title_chlng_shadow{-webkit-box-shadow: 0px 16px 52px 7px rgba(5,41,247,0.75);-moz-box-shadow: 0px 16px 52px 7px rgba(5,41,247,0.75);box-shadow: 0px 16px 52px 7px rgba(5,41,247,0.75);}.div_thumb_nochlng_shadow{-webkit-box-shadow: 0px 0px 52px 7px rgba(5,41,247,0.75);-moz-box-shadow: 0px 0px 52px 7px rgba(5,41,247,0.75);box-shadow: 0px 0px 52px 7px rgba(5,41,247,0.75);}</style>";
var HTML_NOCHLNG_IDLE = "<body><div class=\"div_marker\"><div class=\"div_thumb\" align=center><img src=\"?\" class=\"img_thumb\"/><img src=\"?\" class=\"img_emoji\"/></div></div></body>";
var HTML_CHLNG_IDLE = "<body><div class=\"div_marker\"><div class=\"div_title\"><p>?</p></div><div class=\"div_thumb\" align=center><img src=\"?\" class=\"img_thumb\"/><img src=\"?\" class=\"img_emoji\"/></div></div></body>";
var HTML_NOCHLNG_SELECTED = "<body><div class=\"div_marker\"><div class=\"div_thumb div_thumb_nochlng_sel\" align=center><img src=\"?\" class=\"img_thumb\"/><img src=\"?\" class=\"img_emoji\"/></div></div></body>";
var HTML_CHLNG_SELECTED = "<body><div class=\"div_marker\"><div class=\"div_title div_title_chlng_shadow\"><p>?</p></div><div class=\"div_thumb div_thumb_chlng_shadow\" align=center><img src=\"?\" class=\"img_thumb\"/><img src=\"?\" class=\"img_emoji\"/></div></div></body>";

function RemoveMarker(marker)
{
    // Start destroying the html drawables
    marker.markerObject.drawables.removeCamDrawable([marker.markerDrawable_idle, marker.markerDrawable_selected]);
    marker.markerObject.drawables.removeIndictorDrawable(marker.directionIndicatorDrawable);
    marker.markerObject.drawables.removeIndictor(marker.radardrawables);

    marker.markerDrawable_idle.destroy();
    marker.markerDrawable_selected.destroy();
    marker.markerDrawable_directionIndicator.destroy();
    marker.markerObject.destroy();
}

function Marker(poiData) {

    this.poiData = poiData;
    this.isSelected = false;

    /*
        With AR.PropertyAnimations you are able to animate almost any property of ARchitect objects. This sample will animate the opacity of both background drawables so that one will fade out while the other one fades in. The scaling is animated too. The marker size changes over time so the labels need to be animated too in order to keep them relative to the background drawable. AR.AnimationGroups are used to synchronize all animations in parallel or sequentially.
    */
    this.animationGroup_idle = null;
    this.animationGroup_selected = null;

    // create the AR.GeoLocation from the poi data
    var markerLocation = new AR.GeoLocation(poiData.latitude, poiData.longitude, poiData.altitude);

    // Load the appropriate html coded page /**/ 
    var htmlUri_idle = (poiData.type == DROP_TYPE_CHALLENGE) ? buildHtmlCode_idle(true,poiData) : buildHtmlCode_idle(false,poiData);
    var htmlUri_selected = (poiData.type == DROP_TYPE_CHALLENGE) ? buildHtmlCode_selected(true, poiData) : buildHtmlCode_selected(false, poiData);

    // create an AR.HtmlDrawable for the marker in idle state
    this.markerDrawable_idle = new AR.HtmlDrawable({ html: htmlUri_idle }, World.geoObjectsScale, {
        offsetX: 0,
        renderingOrder: 20,
        horizontalAnchor: AR.CONST.HORIZONTAL_ANCHOR.CENTER,
        opacity: 1,
        viewportWidth: 220,
        updateRate: AR.HtmlDrawable.UPDATE_RATE.STATIC,
        onClick: Marker.prototype.getOnClickTrigger(this)
    });

    
    // create an AR.HtmlDrawable for the marker in selected state
    this.markerDrawable_selected = new AR.HtmlDrawable({ html: htmlUri_selected }, World.geoObjectsScale, {
        offsetX: 0,
        renderingOrder: 20, //TODO : CONST
        horizontalAnchor: AR.CONST.HORIZONTAL_ANCHOR.CENTER,
        viewportWidth: 220,
        updateRate: AR.HtmlDrawable.UPDATE_RATE.STATIC,
        opacity: 0.0,
        onClick:null
    });

    

    /*
        Create an AR.ImageDrawable using the AR.ImageResource for the direction indicator which was created in the World. Set options regarding the offset and anchor of the image so that it will be displayed correctly on the edge of the screen.
    */
    this.directionIndicatorDrawable = new AR.ImageDrawable(World.markerDrawable_directionIndicator, 0.1, {
        enabled: false,
        verticalAnchor: AR.CONST.VERTICAL_ANCHOR.TOP
    });

    /*
        The representation of an AR.GeoObject in the radar is defined in its drawables set (second argument of AR.GeoObject constructor). 
        Once drawables.radar is set the object is also shown on the radar e.g. as an AR.Circle
    */
    this.radarCircle = new AR.Circle(0.03, {
        horizontalAnchor: AR.CONST.HORIZONTAL_ANCHOR.CENTER,
        opacity: 0.8,
        style: {
            fillColor: "#ffffff"
        }
    });

    /*
        Additionally create circles with a different color for the selected state.
    */
    this.radarCircleSelected = new AR.Circle(0.05, {
        horizontalAnchor: AR.CONST.HORIZONTAL_ANCHOR.CENTER,
        opacity: 0.8,
        style: {
            fillColor: "#0066ff"
        }
    });

    this.radardrawables = [];
    this.radardrawables.push(this.radarCircle);

    this.radardrawablesSelected = [];
    this.radardrawablesSelected.push(this.radarCircleSelected);

    /*  
        Note that indicator and radar-drawables were added
    */
    this.markerObject = new AR.GeoObject(markerLocation, {
        drawables: {
            cam: [this.markerDrawable_idle, this.markerDrawable_selected],
            indicator: this.directionIndicatorDrawable,
            radar: this.radardrawables
        }
    });

    return this;
}

function buildHtmlCode_idle(isChallenge, poiData) {

    var html = HTML_NOCHLNG_IDLE;
    if (isChallenge) {
        html = HTML_CHLNG_IDLE.replace("?", poiData.title);
    }

    html = html.replace("?", poiData.thumbnail);
    html = html.replace("?", poiData.emoji);

    return HTML_HEADER + HTML_STYLE_IDLE + html;
}


function buildHtmlCode_selected(isChallenge, poiData) {

    var html = HTML_NOCHLNG_SELECTED;
    if (isChallenge) {
        html = HTML_CHLNG_SELECTED.replace("?", poiData.title);
    }

    html = html.replace("?", poiData.thumbnail);
    html = html.replace("?", poiData.emoji);

    return HTML_HEADER + HTML_STYLE_IDLE + HTML_STYLE_SELECTED + html;
}

Marker.prototype.getOnClickTrigger = function (marker) {

    /*
        The setSelected and setDeselected functions are prototype Marker functions. 
        Both functions perform the same steps but inverted.
    */

    return function () {
        if (!Marker.prototype.isAnyAnimationRunning(marker)) {

            if (marker.isSelected) {

                Marker.prototype.setDeselected(marker);

            } else {
                Marker.prototype.setSelected(marker);
                try {
                    World.onMarkerSelected(marker);
                } catch (err) {
                    alert(err);
                }

            }
        } else {
            AR.logger.debug('a animation is already running');
        }


        return true;
    };
};

/*
    Property Animations allow constant changes to a numeric value/property of an object, dependent on start-value, end-value and the duration of the animation. Animations can be seen as functions defining the progress of the change on the value. The Animation can be parametrized via easing curves.
*/

Marker.prototype.setSelected = function (marker) {

    marker.isSelected = true;

    if (marker.animationGroup_selected === null) {

        // create AR.PropertyAnimation that animates the opacity to 0.0 in order to hide the idle-state-drawable
        var hideIdleDrawableAnimation = new AR.PropertyAnimation(marker.markerDrawable_idle, "opacity", null, 0.0, kMarker_AnimationDuration_ChangeDrawable);
        // create AR.PropertyAnimation that animates the opacity to 1.0 in order to show the selected-state-drawable
        var showSelectedDrawableAnimation = new AR.PropertyAnimation(marker.markerDrawable_selected, "opacity", null, 1.0, kMarker_AnimationDuration_ChangeDrawable);

        // create AR.PropertyAnimation that animates the scaling of the idle-state-drawable to 1.2
        var idleDrawableResizeAnimation = new AR.PropertyAnimation(marker.markerDrawable_idle, 'scaling', null, 1.2, kMarker_AnimationDuration_Resize, new AR.EasingCurve(AR.CONST.EASING_CURVE_TYPE.EASE_OUT_ELASTIC, {
            amplitude: 2.0
        }));
        // create AR.PropertyAnimation that animates the scaling of the selected-state-drawable to 1.2
        var selectedDrawableResizeAnimation = new AR.PropertyAnimation(marker.markerDrawable_selected, 'scaling', null, 1.1, kMarker_AnimationDuration_Resize, new AR.EasingCurve(AR.CONST.EASING_CURVE_TYPE.EASE_OUT_ELASTIC, {
            amplitude: 2.0
        }));

        /*
            There are two types of AR.AnimationGroups. Parallel animations are running at the same time, sequentials are played one after another. This example uses a parallel AR.AnimationGroup.
        */
        marker.animationGroup_selected = new AR.AnimationGroup(AR.CONST.ANIMATION_GROUP_TYPE.PARALLEL, [hideIdleDrawableAnimation, showSelectedDrawableAnimation, idleDrawableResizeAnimation]);
    }

    // removes function that is set on the onClick trigger of the idle-state marker
    marker.markerDrawable_idle.onClick = null;
    // sets the click trigger function for the selected state marker
    marker.markerDrawable_selected.onClick = Marker.prototype.getOnClickTrigger(marker);

    // enables the direction indicator drawable for the current marker
    marker.directionIndicatorDrawable.enabled = true;

    marker.markerObject.drawables.radar = marker.radardrawablesSelected;

    // starts the selected-state animation
    marker.animationGroup_selected.start();
};

Marker.prototype.setDeselected = function (marker) {

    marker.isSelected = false;

    marker.markerObject.drawables.radar = marker.radardrawables;


    if (marker.animationGroup_idle === null) {

        // create AR.PropertyAnimation that animates the opacity to 1.0 in order to show the idle-state-drawable
        var showIdleDrawableAnimation = new AR.PropertyAnimation(marker.markerDrawable_idle, "opacity", null, 1.0, kMarker_AnimationDuration_ChangeDrawable);
        // create AR.PropertyAnimation that animates the opacity to 0.0 in order to hide the selected-state-drawable
        var hideSelectedDrawableAnimation = new AR.PropertyAnimation(marker.markerDrawable_selected, "opacity", null, 0, kMarker_AnimationDuration_ChangeDrawable);
        // create AR.PropertyAnimation that animates the scaling of the idle-state-drawable to 1.0
        var idleDrawableResizeAnimation = new AR.PropertyAnimation(marker.markerDrawable_idle, 'scaling', null, 1.0, kMarker_AnimationDuration_Resize, new AR.EasingCurve(AR.CONST.EASING_CURVE_TYPE.EASE_OUT_ELASTIC, {
            amplitude: 2.0
        }));
        // create AR.PropertyAnimation that animates the scaling of the selected-state-drawable to 1.0
        var selectedDrawableResizeAnimation = new AR.PropertyAnimation(marker.markerDrawable_selected, 'scaling', null, 1.0, kMarker_AnimationDuration_Resize, new AR.EasingCurve(AR.CONST.EASING_CURVE_TYPE.EASE_OUT_ELASTIC, {
            amplitude: 2.0
        }));

        /*
            There are two types of AR.AnimationGroups. Parallel animations are running at the same time, sequentials are played one after another. This example uses a parallel AR.AnimationGroup.
        */
        marker.animationGroup_idle = new AR.AnimationGroup(AR.CONST.ANIMATION_GROUP_TYPE.PARALLEL, [showIdleDrawableAnimation, hideSelectedDrawableAnimation, idleDrawableResizeAnimation, selectedDrawableResizeAnimation]);
    }

    // sets the click trigger function for the idle state marker
    marker.markerDrawable_idle.onClick = Marker.prototype.getOnClickTrigger(marker);
    // removes function that is set on the onClick trigger of the selected-state marker
    marker.markerDrawable_selected.onClick = null;

    // disables the direction indicator drawable for the current marker
    marker.directionIndicatorDrawable.enabled = false;
    // starts the idle-state animation
    marker.animationGroup_idle.start();
};

Marker.prototype.isAnyAnimationRunning = function (marker) {

    if (marker.animationGroup_idle === null || marker.animationGroup_selected === null) {
        return false;
    } else {
        if ((marker.animationGroup_idle.isRunning() === true) || (marker.animationGroup_selected.isRunning() === true)) {
            return true;
        } else {
            return false;
        }
    }
};