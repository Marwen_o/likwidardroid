﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Json;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Android.Locations;
using Wikitude.Architect;

using Xamarin.Forms;
using LikwidAR.Droid.Utils;

namespace LikwidAR.Droid
{
    [Activity(Label = "BasicArchitectActivity")]
    public class BasicArchitectActivity : Activity, ILocationListener, ArchitectView.ISensorAccuracyChangeListener, ArchitectView.IArchitectUrlListener
    {
        public const string WIKITUDE_SDK_KEY = "Yxnn2ByCWvyVueHB4qF5Bhk200nT/YOazfXK3u3VH1uZqXaPJi0z3QGD78l7C9IN+9EBNHFB0FH0fypZ6i7MDgwlnP7vgmr735p7Ra4TuVY7MYGqvwePfdMlVZWqOmBDtcQGN6KSnMDTXw9AN7gBt3PTzIJ5vgQven3MnpqDHQdTYWx0ZWRfX3Rx6XbafJ1s6TWdGyXGWoiejWgBsmdylW9EsHoTLM+2G9Qog0I6gqqOs0JLeXQjvt3uoOwUpYJ/MNpDbZkfW+o1dT8hWWh6Z4yDbRsXQY5DMrPn0L2Ma+P79aTnLA0F5m9Nayx2eD8A6LHXU2me3DvTEdRe7yU4ugBhiLrvJ6wrdKYy+fcHjyQG0EGb4I+3Cx8ugh5p4GSP8zkh/3tzLiokfkW+4W0XsC1/PbMTOmGNzhb+bQA13N2rIa/mnHdaDLstEUSe3KZzPOmqaNRaztbCVV8RnSsB/R7Q9xLljbRUMEfvqP2z+yh+x0ekaEKxVmpQM6K6GvjJn2aWdm4XlzWK6df2SYxbBR1jpeMATvEliw0G3F/RqXTJz4aJOAvyeqq9zxYb06X/Xar5U0B08V/4g6Z2eiag0W3IjO2oMIqI/a/kI1zYdWL6Aej6nLW3iqC56HDZJ2pnF0fkbm7SocPxTOnhGUMkW1kT+04tmNCoJGlSKXykb90=";
        public const string LOG_TAG = "WIKITUDE-EXAMPLE";

        public const string EXTRAS_KEY_ACTIVITY_TITLE_STRING = "activityTitle";
        public const string EXTRAS_KEY_ACTIVITY_ARCHITECT_WORLD_URL = "activityArchitectWorldUrl";
        private const string SAMPLE_WORLD_URL = "samples/5_Browsing$Pois_3_Limiting$Range/index.html";

        protected ArchitectView architectView;
        protected WikitudeHandler WTRequestHandler;
        
        public ILocationProvider locationProvider;
        public Location lastKnownLocation;

        public double screenWidth = Android.Content.Res.Resources.System.DisplayMetrics.WidthPixels / Android.Content.Res.Resources.System.DisplayMetrics.Density;
        public double screenHeight = Android.Content.Res.Resources.System.DisplayMetrics.HeightPixels / Android.Content.Res.Resources.System.DisplayMetrics.Density;


        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here
            this.VolumeControlStream = Android.Media.Stream.Music;

            SetContentView(Resource.Layout.sample_cam);

            var title = "Test World";

            if (Intent.Extras != null && Intent.Extras.Get(EXTRAS_KEY_ACTIVITY_TITLE_STRING) != null)
                title = Intent.Extras.GetString(EXTRAS_KEY_ACTIVITY_TITLE_STRING);

            Title = title;
            architectView = FindViewById<ArchitectView>(Resource.Id.architectView);

            // Architect's world startupConfiguration
            StartupConfiguration startupConfiguration = new StartupConfiguration(WIKITUDE_SDK_KEY, StartupConfiguration.Features.Geo);
            architectView.OnCreate(startupConfiguration);
            WTRequestHandler = new WikitudeHandler(architectView);

            // Setup Resources Providers for the Architect World
            this.locationProvider = new LocationProvider(this, this);
            this.architectView.RegisterUrlListener(this);
            this.architectView.RegisterSensorAccuracyChangeListener(this);
        }

        protected override void OnResume()
        {
            base.OnResume();

            if (architectView != null)
                architectView.OnResume();

            if (locationProvider != null)
                locationProvider.OnResume();
        }

        protected override void OnPause()
        {
            base.OnPause();

            if (architectView != null)
                architectView.OnPause();

            if (locationProvider != null)
                locationProvider.OnPause();


        }

        protected override void OnStop()
        {
            base.OnStop();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            if (architectView != null)
            {
                architectView.UnregisterSensorAccuracyChangeListener(this);

                architectView.OnDestroy();
            }
        }

        public override void OnLowMemory()
        {
            base.OnLowMemory();

            if (architectView != null)
                architectView.OnLowMemory();
        }

        protected override void OnPostCreate(Bundle savedInstanceState)
        {
            base.OnPostCreate(savedInstanceState);

            if (architectView != null)
                architectView.OnPostCreate();

            try
            {
                architectView.Load(SAMPLE_WORLD_URL);
                WTRequestHandler.SetupArchitectWorld();
            }
            catch (Exception ex)
            {
                Log.Error("WIKITUDE_SAMPLE", ex.ToString());
            }
        }

        #region ILocationListener implementation

        public void OnLocationChanged(Location location)
        {
            if (location != null)
                lastKnownLocation = location;

            if (location.HasAltitude)
                architectView.SetLocation(location.Latitude, location.Longitude, location.Altitude, location.HasAccuracy ? location.Accuracy : 1000);
            else
                architectView.SetLocation(location.Latitude, location.Longitude, location.HasAccuracy ? location.Accuracy : 1000);
        }

        public void OnProviderDisabled(string provider)
        {
        }

        public void OnProviderEnabled(string provider)
        {
        }

        public void OnStatusChanged(string provider, Availability status, Bundle extras)
        {

        }
        #endregion


        #region ISensorAccuracyChangeListener implementation
        public void OnCompassAccuracyChanged(int accuracy)
        {
            /* UNRELIABLE = 0, LOW = 1, MEDIUM = 2, HIGH = 3 */
            if (accuracy < 2 && !this.IsFinishing)
                Toast.MakeText(this, Resource.String.compass_accuracy_low, ToastLength.Long).Show();
        }
        #endregion


        #region IArchitectUrlListener implementation

        public bool UrlWasInvoked(string uriString)
        {
            WTRequestHandler.handleRequests(uriString,lastKnownLocation);
            return true;
        }
        #endregion
    }
}

