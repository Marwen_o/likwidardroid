﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using Android.Util;
using Java.IO;
using Wikitude.Architect;

using Xamarin.Forms;

namespace LikwidAR.Droid
{
    [Activity(Label = "Wikitude Samples", MainLauncher=true)]

    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Xamarin.Forms.Forms.Init(this, bundle);

            App app = new App();

            app.screenWidth = Resources.DisplayMetrics.WidthPixels / Resources.DisplayMetrics.Density;
            app.screenWidth = Resources.DisplayMetrics.HeightPixels / Resources.DisplayMetrics.Density;
            LoadApplication(app);

        }
    }
}


