﻿using Android.App;
using LikwidAR;
using LikwidAR.Droid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Page2), typeof(PageRendered))]
namespace LikwidAR.Droid
{
    public class PageRendered : PageRenderer
    {
        public PageRendered()
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Page> e)
        {
            base.OnElementChanged(e);

            // this is a ViewGroup - so should be able to load an AXML file and FindView<>
            Context.StartActivity(typeof(BasicArchitectActivity));
        }
    }
}