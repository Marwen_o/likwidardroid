using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Json;
using Android.Locations;

namespace LikwidAR.Droid.Utils
{
    public class DropsLoader
    {
        public string jsonData = "[{\"Id\": 1,\"Type\": \"Challenge\",\"Title\": \"SICK TRICK CONTEST\",\"Thumbnail\": \"http://fr.fordesigner.com/imguploads/Image/cjbc/zcool/png20080526/1211812546.png\",\"Emoji\": \"http://www.pngall.com/wp-content/uploads/2016/06/Fearful-Emoji-PNG-180x180.png\",\"Latitude\": 36.835848,\"Longitude\": 10.238968,\"Altitude\": 2},{\"Id\": 2,\"Type\": \"Image\",\"Title\": \"SELFIE EVENT\",\"Thumbnail\": \"http://fr.fordesigner.com/imguploads/Image/cjbc/zcool/png20080526/1211812546.png\",\"Emoji\": \"http://www.pngall.com/wp-content/uploads/2016/06/Face-with-Tears-of-Joy-Emoji-PNG-180x180.png\",\"Latitude\": 36.875635,\"Longitude\": 10.278970999999992,\"Altitude\": 30},{\"Id\": 3,\"Type\": \"Video\",\"Title\": \"RACE EVENT\",\"Thumbnail\" : \"http://www.clipartkid.com/images/715/sphere-shape-clipart-sphere-clipart-YirJcl-clipart.png\",\"Emoji\": \"http://www.pngall.com/wp-content/uploads/2016/06/Grinning-Face-Emoji-PNG.png\",\"Latitude\": 36.845635,\"Longitude\": 10.248970999999992,\"Altitude\": 60},{\"Id\": 4,\"Type\": \"Image\",\"Title\": \"CHALLENGE SELFIE\",\"Thumbnail\": \"http://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Cercle_rouge_100%25.svg/200px-Cercle_rouge_100%25.svg.png\",\"Emoji\": \"http://www.pngall.com/wp-content/uploads/2016/06/Love-Hearts-Eyes-Emoji-PNG-180x180.png\",\"Latitude\": 36.865635,\"Longitude\": 10.268970999999992,\"Altitude\": 10}]";


        public DropsLoader() {}

        // Retrieving Pois Data
        internal string getPoisData(Location location)
        {
            return getPoisDataFromServer(location);
        }

            // From Server
        protected string getPoisDataFromServer(Location location)
        {
            return jsonData;
        }
        
            // From Local Random generation
        protected string createRandomPoisData(Location location)
        {
            return GetPoiInformation(location,20).ToString();
        }

        public static JsonArray GetPoiInformation(Location userLocation, int numberOfPlaces)
        {
            if (userLocation == null)
                return null;

            var pois = new List<JsonObject>();

            for (int i = 0; i < numberOfPlaces; i++)
            {
                var loc = GetRandomLatLonNearby(userLocation.Latitude, userLocation.Longitude);
                Random rnd = new Random();
                int t = rnd.Next(0, 3);
                var type = (t == 0) ? "Challenge" : ((t == 1) ? "Video" : "Image");
                var p = new Dictionary<string, JsonValue>(){
                    { "Id", i.ToString() },
                    { "Title", "POI#" + i.ToString() },
                    { "Type", type.ToString() },
                    { "Thumbnail", "This is the description of POI#" + i.ToString() },
                    { "Emoji", "This is the description of POI#" + i.ToString() },
                    { "Latitude", loc[0] },
                    { "Longitude", loc[1] },
                    { "Latitude", 100f }
                };


                pois.Add(new JsonObject(p.ToList()));
            }

            var vals = from p in pois select (JsonValue)p;

            return new JsonArray(vals);
        }

        static double[] GetRandomLatLonNearby(double lat, double lon)
        {
            var rnd = new Random();

            var newLat = lat + rnd.NextDouble() / 5 - 0.1;
            var newLng = lon + rnd.NextDouble() / 5 - 0.1;

            return new double[] { newLat, newLng };
        }



        // Dispose From the DropsLoader
        internal void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}