using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Locations;

using Wikitude.Architect;
using System.Json;

namespace LikwidAR.Droid.Utils
{
    public class WikitudeHandler
    {
        // CONSTANTS
        private DropsLoader dropLoader;
        private ArchitectView architectView;


        public WikitudeHandler(ArchitectView architectView)
        {
            this.architectView = architectView;
            this.dropLoader = new DropsLoader();
        }


        // Answer ArchitectWorld's Requests
        public void onMarkerSelected(int id)
        {
            var js = "alert(\"" + id + "\")";
            this.architectView.CallJavascript(js);
        }
        
        public void loadPoisInArchitectWorld(Location location)
        {
            string jsonData = dropLoader.getPoisData(location);
            var js = "World.loadPoisFromJsonData(" + jsonData.ToString() + ")";
            this.architectView.CallJavascript(js);
        }

        public void SetupArchitectWorld()
        {
            string setup = getArchitectWorldSetups();
            var js = "World.loadSetupsFromJsonData(" + setup.ToString() + ")";
            this.architectView.CallJavascript(js);
        }



        // Build Setup Json
        private string getArchitectWorldSetups()
        {
            // Build The Json array containing the ArchitectWorld setups
            var setup = new JsonObject();

            // screen Scale
            double screenWidth = Android.Content.Res.Resources.System.DisplayMetrics.WidthPixels / Android.Content.Res.Resources.System.DisplayMetrics.Density;
            setup.Add("screenwidth", screenWidth);

            return setup.ToString();
        }
        

        // Handle Wikitude url invokes
        public void handleRequests(string uriString,Location location)
        {
            var invokedUri = Android.Net.Uri.Parse(uriString);

            if (invokedUri.Authority == "markerselected")
            {
                Int32 id;
                if (Int32.TryParse(invokedUri.GetQueryParameter("id"), out id))
                    onMarkerSelected(id);
            }
            else if (invokedUri.Authority == "requestdata")
            {
                loadPoisInArchitectWorld(location);
            }
            else if (invokedUri.Authority == "requestsetup")
            {
                SetupArchitectWorld();
            }
        }

    }
}