﻿using System;
using Xamarin.Forms;

namespace LikwidAR
{
	public partial class App : Application
	{
        public double screenWidth;
        public double screenHeight;

        public App()
		{
			InitializeComponent();

			MainPage = new NavigationPage(new Page1 (screenWidth,screenHeight)) ;
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}



        public void toIdPage(string arg2)
        {
            MainPage = new Page3(arg2);
        }
    }
}
