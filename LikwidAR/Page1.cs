﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace LikwidAR
{
    public class Page1 : ContentPage
    {
        double screenWidth, screenHeight;

        public Page1(double Wscr,double Hscr)
        {
            screenWidth = Wscr;
            screenHeight = Hscr;

            Label header = new Label
            {
                Text = "LIKWID",
                HorizontalOptions = LayoutOptions.Center
            };

            Button button = new Button
            {
                Text = "Go to AR",
                Font = Font.SystemFontOfSize(NamedSize.Large),
                BorderWidth = 1,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };

            button.Clicked += OnButtonClicked;

            Content = new StackLayout
            {
                Children = {
                    header,
                    button
                }
            };
        }

        void OnButtonClicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new Page2(screenWidth,screenHeight));
        }
    }
}
