﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

using MonoTouch.Dialog;
using System.IO;
using System.Json;

namespace LikwidAR.iOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	[Register(\"AppDelegate\")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations
		public override UIWindow Window { get; set;}
		UINavigationController navController;
		ARViewController viewController;

		//
		// This method is invoked when the application has loaded and is ready to run. In this
		// method you should instantiate the window, load the UI into it and then make the window
		// visible.
		//
		// You have 17 seconds to return from this method, or iOS will terminate your application.


		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			//Set the Path Of the Sample to run to the variable examplePath
			//Either \"3_PointOfInterest_1_PoiAtLocation\" to run the POI Sample
			//Or \"5_BrowsingPois_2_AddingRadar\" to run Radar Sample
			var examplePath = \"5_BrowsingPois_2_AddingRadar\";

			//Initialize a UIWindow 
			Window = new UIWindow(UIScreen.MainScreen.Bounds);

			//Initialize an Augmented Reality View Controller instance
			viewController = new ARViewController(examplePath,false);

			//
			navController = new UINavigationController(viewController);

			//Set the Controller View associated to the AR World View as root view of the application
			Window.RootViewController = navController;

			// make the window visible
			Window.MakeKeyAndVisible();

			return true;
		}

	}
}
