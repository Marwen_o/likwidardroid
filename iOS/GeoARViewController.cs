﻿using System;

using Foundation;
using UIKit;

using Wikitude.Architect;

namespace LikwidAR.iOS
{
	public partial class ArchitectViewDelegate : WTArchitectViewDelegate
	{
		public override void InvokedURL(WTArchitectView architectView, NSUrl url)
		{
			Console.WriteLine("architect view invoked url: " + url);
		}

		public override void DidFinishLoadNavigation(WTArchitectView architectView, WTNavigation navigation)
		{
			Console.WriteLine("architect view loaded navigation: " + navigation.OriginalURL);
		}

		public override void DidFailToLoadNavigation(WTArchitectView architectView, WTNavigation navigation, NSError error)
		{
			Console.WriteLine("architect view failed to load navigation. " + error.LocalizedDescription);
		}
	}

	public partial class GeoARViewController : UIViewController
	{

		private WTArchitectView architectView;
		private WTNavigation navigation;
		private ArchitectViewDelegate architectViewDelegate = new ArchitectViewDelegate();


		public GeoARViewController(IntPtr handle) : base(handle)
		{
		}

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning();

			// Release any cached data, images, etc that aren't in use.
		}

		public void StartAR()
		{
			if (!architectView.IsRunning)
			{
				architectView.Start((startupConfiguration) =>
			   {
				   // use startupConfiguration.CaptureDevicePosition = AVFoundation.AVCaptureDevicePosition.Front; to start the Wikitude SDK with an active front cam
				   startupConfiguration.CaptureDevicePosition = AVFoundation.AVCaptureDevicePosition.Back;
			   }, (isRunning, error) =>
			   {
				   if (isRunning)
				   {
					   Console.WriteLine("Wikitude SDK version " + WTArchitectView.SDKVersion + " is running.");
				   }
				   else
				   {
					   Console.WriteLine("Unable to start Wikitude SDK. Error: " + error.LocalizedDescription);
				   }
			   });
			}
		}

		public void StopAR()
		{
			if (architectView.IsRunning)
			{
				architectView.Stop();
			}
		}

		#region View lifecycle

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.

			this.architectView = new Wikitude.Architect.WTArchitectView();
			this.architectView.Delegate = architectViewDelegate;
			this.View.AddSubview(this.architectView);
			this.architectView.TranslatesAutoresizingMaskIntoConstraints = false;

			NSDictionary views = new NSDictionary(new NSString("architectView"), architectView);
			this.View.AddConstraints(NSLayoutConstraint.FromVisualFormat("|[architectView]|", 0, null, views));
			this.View.AddConstraints(NSLayoutConstraint.FromVisualFormat("V:|[architectView]|", 0, null, views));

			architectView.SetLicenseKey("Ug7LmdNQJKN5AX85OMRGt1LIk1xBmwOWi8u+4eEVBpIvKZ8EWh2w1z3f5Cd4YHnWlEIdazY1envd/W7Xy5U4GUlkNmH2l9ddWZr5gIsz0zuD4GZVunmt0o49f4rDv+ssM78CAklidZeMkxqTGGoG6I8UjoegiWEKtzoH3qWpruNTYWx0ZWRfX3mWnNyLFq5Z+rfkc+m7sBeEhO/Urh2wYX/E57J6MdWPrCvmW0Zrt0RfAkUjmmHZ9MdjOyghN4VtSnY6nwc+Xz2Wg8vrCG02TIMw8SbNBRqP4ljrg3BnmjSONHRC69rzLnzCalB+YXAIdh5QdZI8TG8nJNUQCZGmjdrF5SpznSbcLpjDqfI36NaGW3cnH6evloXrcItbrnJDeeZlfB7CZj6PpLaf6q4GpKJRIEiVbeY3UpQ19+5IsydEbo0eVwZQFtE/G/NB7mNM1SjwteJ53EumNT9hd/4fMmk7L3nUj4kpyZ2gttPTS0/1kxtwVJjfRntngMiSN6czJrmrI5IyqN3qjEDextUNJ6zpvj97Vx/k+6RkItCgbMLZzdGgnyvIq9jumKiICOZXcFz4iacFFsYag4w87FoUwJFdp2SAsuW374FdmMB2tE5Zk5CONbQvMCKkMwdT6RnqA0SrzX4NA9qDLv8DwcoOu3jiszRE//8uOGS26I4NIznQniu8gn04sdeDm3P50rVkB7Vq3CDP89LIE8CoqChJ9DVEm2IzwfHFRd6cDalxC9szRKxOI4H5Z6wJY4tPHTeQha3Gp4jFQXLbtwfPLPcr+DyH8GkOf6+aIbzz3AVZZz9v67JN2W5O1xR2BZXAjkE0SC4zXK2g0H6MuDEYLdLHZCnl8ik/Aw3ydyFe5zw9+olNC/72uH5rA5ZLyiACVauyXwwsc6bNzJu3c5Dyb724zg==");

			NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.DidBecomeActiveNotification, (notification) =>
			{
				if (navigation.Interrupted)
				{
					architectView.reloadArchitectWorld();
				}
				StartAR();
			});

			NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.WillResignActiveNotification, (notification) =>
			{
				StopAR();
			});

			var path = NSBundle.MainBundle.BundleUrl.AbsoluteString + "3_PointOfInterest_1_PoiAtLocation/index.html";
			navigation = architectView.LoadArchitectWorldFromURL(NSUrl.FromString(path), Wikitude.Architect.WTFeatures.Geo);
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			StartAR();
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);

			StopAR();
		}

		#endregion

		#region Rotation

		public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
		{
			base.WillRotate(toInterfaceOrientation, duration);

			architectView.SetShouldRotateToInterfaceOrientation(true, toInterfaceOrientation);
		}

		public override bool ShouldAutorotate()
		{
			return true;
		}

		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations()
		{
			return UIInterfaceOrientationMask.All;
		}

		#endregion
	}
}

